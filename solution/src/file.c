#include "file.h"

#include <stdio.h>
#include <stdlib.h>

FILE* open_file(const char* path, const char* arg) { return fopen(path, arg); }

void close_file(FILE* descriptor) { fclose(descriptor); }

enum read_status read_data(FILE* descriptor, size_t* size, char** data) {
  fseek(descriptor, 0, SEEK_END);
  size_t fileSize = ftell(descriptor);
  rewind(descriptor);

  *data = malloc(fileSize);
  if (*data == NULL) {
    return BUFFER_SIZE_ERROR;
  }
  *size = fread(*data, 1, fileSize, descriptor);
  if (*size != fileSize) {
    free(*data);
    return READ_SIZE_ERROR;
  }
  return READ_OK;
}

enum write_status write_data(FILE* descriptor, const char* data, size_t size) {
  if (descriptor == NULL || data == NULL || size <= 0) {
    return DATA_LOSS;
  }
  size_t bytesWritten = fwrite(data, 1, size, descriptor);

  if (bytesWritten != size) {
    return LESS_WRITED;
  }
  return WRITE_OK;
}
