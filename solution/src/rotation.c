
#include "rotation.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "types.h"

#define PIXEL_SIZE sizeof(struct pixel)
#define IMAGE_SIZE sizeof(struct image)
#define index_to_2_dimensions(x, y, width) (((y) * (width)) + (x))

static size_t get_rotated_coords(size_t x, size_t y, int rotation,
                                 const struct image* img) {
  size_t rotX = 0;
  size_t rotY = 0;

  if (rotation > 0) {
    rotX = y;
    rotY = img->width - x - 1;
  } else if (rotation < 0) {
    rotX = img->height - y - 1;
    rotY = x;
  } else {
    rotX = img->width - x - 1;
    rotY = img->height - y - 1;
    return index_to_2_dimensions(rotX, rotY, img->width);
  }
  return index_to_2_dimensions(rotX, rotY, img->height);
}

static void rotate_dg(struct image* rotated_image, const struct image* img,
                      int rotation) {
  for (size_t y = 0; y < rotated_image->height; y++)
    for (size_t x = 0; x < rotated_image->width; x++) {
      size_t new_pixel_coord = get_rotated_coords(x, y, rotation, img);
      size_t old_pixel_coord = index_to_2_dimensions(x, y, img->width);
      rotated_image->data[new_pixel_coord] = img->data[old_pixel_coord];
    }
  if (rotation != 0) {
    rotated_image->height = img->width;
    rotated_image->width = img->height;
  }
}

struct image* rotate(const struct image* img, int degrees) {
  const size_t image_pixel_size = img->height * img->width;
  const size_t image_byte_size = image_pixel_size * PIXEL_SIZE;
  struct image* rotated_image = malloc(IMAGE_SIZE);

  rotated_image->width = img->width;
  rotated_image->height = img->height;
  rotated_image->data = malloc(image_byte_size);

  const int rotation = degrees / 90;
  if (rotation != 0 && abs(rotation) != 4) {
    int rotate_arg = -1;
    if (rotation % 2 == 0) rotate_arg = 0;
    if (rotation == 1 || rotation == -3) rotate_arg = 1;

    rotate_dg(rotated_image, img, rotate_arg);
    return rotated_image;
  }

  memcpy(rotated_image->data, img->data, image_byte_size);  // NOLINT
  return rotated_image;
}
