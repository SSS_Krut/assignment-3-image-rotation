#include <stdio.h>
#include <stdlib.h>

#include "bmp.h"
#include "file.h"
#include "rotation.h"
#include "types.h"
#define _error_check(value, check, value_expected, message, valret) \
  if (value check value_expected) {                                 \
    fprintf(stderr, message);                                       \
    valret;                                                         \
  }

int main(int argc, char** argv) {
  _error_check(argc, <, 4, "Not enough arguments", return 1);

  FILE* image_old_file = open_file(argv[1], "r+");
  _error_check(image_old_file, ==, NULL, "Error with opening image", return 1);

  size_t image_data_size = 0;
  char* buffer = NULL;
  enum read_status rs = read_data(image_old_file, &image_data_size, &buffer);
  close_file(image_old_file);
  _error_check(rs, !=, READ_OK, "Error with reading data from image", return 1);

  struct image* image_old = from_bmp(buffer, image_data_size);
  free(buffer);
  _error_check(image_old, ==, NULL, "Error with converting from bmp", return 1);

  int degree;
  sscanf(argv[3], "%d", &degree);  // NOLINT
  struct image* image_new = rotate(image_old, degree);
  free(image_old->data);
  free(image_old);
  _error_check(image_new, ==, NULL, "Error with rotation", return 1);

  char* buffer2 = to_bmp(image_new, &image_data_size);
  _error_check(buffer2, ==, NULL, "Error with converting to bmp", return 1);
  free(image_new->data);
  free(image_new);

  FILE* image_new_file = open_file(argv[2], "w+");
  _error_check(image_new_file, ==, NULL, "Error with saving new image",
               close_file(image_new_file);
               return 1);

  enum write_status ws = write_data(image_new_file, buffer2, image_data_size);
  free(buffer2);
  close_file(image_new_file);
  _error_check(ws, !=, WRITE_OK, "Error with writing data to image", return ws);

  return 0;
}
