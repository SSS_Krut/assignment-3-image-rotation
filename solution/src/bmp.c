#include "bmp.h"

#include <stdlib.h>
#include <string.h>

#include "types.h"

#define ALIGNMENT_GAP 4
#define HEADER_SIZE sizeof(struct bmp_header)
#define IMAGE_SIZE sizeof(struct image)
#define PIXEL_SIZE sizeof(struct pixel)
#define BMPH_FILE_TYPE = 0x4d42
#define BMPH_PLANES = 1
#define BMPH_BITS_PER_PIXEL = 24

static size_t get_width_with_padding(size_t width) {
  return ((width + ALIGNMENT_GAP - 1) / ALIGNMENT_GAP) * ALIGNMENT_GAP;
}

static struct bmp_header setup_header(size_t file_size, size_t width,
                                      size_t height) {
  struct bmp_header bmph;

  memset(&bmph, 0, HEADER_SIZE);  // NOLINT
  bmph.bfType = 0x4d42;
  bmph.bfileSize = file_size;
  bmph.bOffBits = HEADER_SIZE;
  bmph.biSize = HEADER_SIZE;
  bmph.biWidth = width;
  bmph.biHeight = height;
  bmph.biPlanes = 1;
  bmph.biBitCount = 24;
  return bmph;
}

struct image* from_bmp(const char* data, size_t size) {
  struct image* img = malloc(IMAGE_SIZE);
  struct bmp_header* bmph = NULL;
  if (size < HEADER_SIZE) {
    free(img);
    free(bmph);
    return NULL;
  }
  bmph = malloc(HEADER_SIZE);
  memcpy(bmph, data, HEADER_SIZE);  // NOLINT

  const char* pixel_data = data + bmph->bOffBits;
  img->width = bmph->biWidth;
  img->height = bmph->biHeight;

  const size_t width_with_padding =
      get_width_with_padding(img->width * PIXEL_SIZE);
  const size_t pixels_number = img->width * img->height;
  img->data = malloc(pixels_number * PIXEL_SIZE);
  for (size_t h = 0; h < bmph->biHeight; h++) {
    for (size_t i = 0; i < bmph->biWidth; i++) {
      const size_t pixel_index_with_padding = i * 3 + h * width_with_padding;
      img->data[i + h * img->width].b = pixel_data[pixel_index_with_padding];
      img->data[i + h * img->width].g =
          pixel_data[pixel_index_with_padding + 1];
      img->data[i + h * img->width].r =
          pixel_data[pixel_index_with_padding + 2];
    }
  }

  free(bmph);
  return img;
}

char* to_bmp(const struct image* img, size_t* size) {
  const size_t width_without_padding = PIXEL_SIZE * img->width;
  const size_t width_with_padding =
      get_width_with_padding(width_without_padding);
  const size_t file_size = HEADER_SIZE + width_with_padding * img->height;
  const size_t padding = width_with_padding - width_without_padding;

  char* buffer = malloc(file_size);
  struct bmp_header bmph = setup_header(file_size, img->width, img->height);

  memcpy(buffer, &bmph, HEADER_SIZE);  // NOLINT

  char* pixel_data_ptr = buffer + HEADER_SIZE;
  char* img_data_ptr = (char*)img->data;
  for (size_t h = 0; h < img->height; h++) {
    memcpy(pixel_data_ptr, img_data_ptr, width_without_padding);  // NOLINT
    memset(pixel_data_ptr + width_without_padding, 0, padding);   // NOLINT
    pixel_data_ptr += width_with_padding;
    img_data_ptr += width_without_padding;
  }
  *size = file_size;

  return buffer;
}
