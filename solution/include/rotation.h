#ifndef ROTATION_H
#define ROTATION_H
#include "types.h"

struct image* rotate(const struct image* img, const int degrees);

#endif
