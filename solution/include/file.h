#ifndef FILE_H
#define FILE_H
#include <stdio.h>

#include "types.h"

FILE* open_file(const char* path, const char* arg);
void close_file(FILE* descriptor);
enum read_status read_data(FILE* descriptor, size_t* size, char** data);
enum write_status write_data(FILE* descriptor, const char* data,
                             const size_t size);

#endif
