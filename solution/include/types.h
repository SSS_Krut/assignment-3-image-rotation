#ifndef TYPES_H
#define TYPES_H
#include <stdint.h>

struct __attribute__((packed)) pixel {
  uint8_t b, g, r;
};

struct image {
  uint64_t width, height;
  struct pixel* data;
};

enum read_status { READ_OK = 0, BUFFER_SIZE_ERROR, READ_SIZE_ERROR };
enum write_status { WRITE_OK = 0, DATA_LOSS, LESS_WRITED };
#endif
